import React from 'react';
import { useQuery } from 'react-query';

import { fetchAbilities } from '../../../../../services'


import { Text } from './styles'

const Ability = ({ name, language }) => {
    const { isLoading, data } = useQuery(`abilitie_${name}`, () => fetchAbilities({ id: name }));

    if (isLoading) return null

    return data.names.map((ability, i) => ability.language.name === language && <Text key={i}>- {ability.name}</Text>)
}

export { Ability }