import React, { useContext } from 'react';
import { useHistory } from "react-router-dom";
import { useQuery } from 'react-query';
import { useParams } from "react-router-dom";

import { Ability } from './components'

import { fetchPokemon, fetchSpecies } from '../../../services'
import { LanguageContext } from '../../../context'

import { Back, Container, DetailsContainer, Header, PokemonImage, PokemonImageContainer, Text, Title, Subtitle } from './styles'

const PokemonDetails = () => {
    const history = useHistory();

    const { id } = useParams();

    const { state: { language } } = useContext(LanguageContext)

    const { isLoading, error, data } = useQuery('pokemon', () => fetchPokemon({ id }));
    const { isLoading: speciesIsLoading, data: speciesData } = useQuery('species', () => fetchSpecies({ id }));

    const goBack = () => history.push('/pokemons');

    if (error) return <p>Is error</p>

    if (isLoading) return <p>Is loading</p>

    return (
        <Container>
            <Header>
                <Back onClick={goBack}>Back</Back>
                <Title>{data.name}</Title>
                <PokemonImageContainer>
                    <PokemonImage src={data.sprites.other["official-artwork"].front_default} />
                </PokemonImageContainer>
            </Header>

            <DetailsContainer>
                <Subtitle>Species</Subtitle>
                {!speciesIsLoading && speciesData?.names.map((specie, i) => specie.language.name === language && <Text key={i}>- {specie.name}</Text>)}

                <Subtitle>Abilities</Subtitle>
                {!isLoading && (data.abilities.map(({ ability }, i) => <Ability key={i} name={ability.name} language={language} />))}
            </DetailsContainer>
        </Container>
    );
}

export { PokemonDetails }