import styled from "styled-components";

export const Back = styled.p`
    color: #000000;
    font-size: 16px;
    font-weight: bold;
    cursor: pointer;
    margin-left: 20px;
    width: fit-content;
`;

export const Container = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    height: 100vh;
    background-color: #000000;
`;

export const DetailsContainer = styled.div`
    display: flex;
    flex: 1.5;
    flex-direction: column;
    // border-top-left-radius: 25px;
    // border-top-right-radius: 25px;
    padding: 50px
`;

export const Header = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    border-bottom-left-radius: 25px;
    border-bottom-right-radius: 25px;
    background-color: #FFFFFF;

`;

export const PokemonImage = styled.img`
    width: 300px;
    @media (max-width: 576px) {
        width: 200px;
    }
`;

export const PokemonImageContainer = styled.div`
    display: flex;
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const Text = styled.p`
    color: #FFFFFF;
    font-size: 16px;
`;

export const Title = styled.p`
    color: #000000;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 36px;
    text-align: center;
`;

export const Subtitle = styled.p`
    color: red;
    font-size: 20px;
    text-transform: uppercase;
    border-bottom: solid red 2px;
    width: fit-content;
`;