import React, { useState } from 'react';
import { useQuery } from 'react-query';
import { useHistory } from "react-router-dom";

import { fetchPokemons } from '../../../services'

import { PokemonCard } from './components'
import { Pagination } from '../../../components'

import { Container, PokemonsContainer } from './styles';

const PokemonsView = () => {
    const history = useHistory();

    const [page, setPage] = useState(0)

    const { isLoading, error, data, isFetching } = useQuery(['pokemons', page], () => fetchPokemons({ limit: 5, offset: page }), { keepPreviousData: true });

    if (error) return <p>Is error</p>

    if (isLoading) return <p>Is loading</p>

    const handleNextPage = () => setPage(old => old + 5);

    const handlePreviousPage = () => setPage(old => Math.max(old - 5, 0));

    const handleRedirectDetails = ({ url }) => {
        const idSpplited = url.replace("https://pokeapi.co/api/v2/pokemon/", "").replace("/", "")
        history.push(`/pokemon-details/${idSpplited}`)
    };

    return (
        <Container>
            <Pagination
                totalDocuments={data.count}
                currentPage={Math.ceil(page / 5 + 1)}
                pageCount={5}
                previousDisabled={page === 0}
                handleNextPage={handleNextPage}
                handlePreviousPage={handlePreviousPage}
            />
            <PokemonsContainer>
                {
                    !isFetching && data.results.map((pokemon, i) => (
                        <PokemonCard key={i} name={pokemon.name} onClick={() => handleRedirectDetails({ url: pokemon.url })} />
                    ))
                }
            </PokemonsContainer>
        </Container>
    );
}

export { PokemonsView }