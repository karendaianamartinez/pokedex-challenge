import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: center;
    min-width: 300px;
    max-width: 300px;
    min-height: 300px;
    margin: 16px;
    border-radius: 8px;
    background-color: #FFFFFF;

    &:hover {
        -webkit-transform: scale(1.06);
        -ms-transform: scale(1.06);
        transform: scale(1.06);
        box-shadow: 2px 2px 2px 1px #FFFFFF;
        cursor: pointer;
    }

    @media (max-width: 576px) {
        min-width: 250px;
        max-width: 250px;
        min-height: 250px;
    }
`;

export const Title = styled.p`
    color: #000000;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 36px;
    text-align: center;

    @media (max-width: 576px) {
        font-size: 30px;
    }
`;
