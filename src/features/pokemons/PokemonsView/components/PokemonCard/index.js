import React from 'react';

import { Container, Title } from './styles'

const PokemonCard = ({ name, onClick }) => {

    return (
        <Container onClick={onClick}>
            <Title>{name}</Title>
        </Container>
    );
}

export { PokemonCard }