import React, { createContext, useReducer } from 'react'

const initialState = {
    language: 'en',
}

const LanguageContext = createContext({ state: initialState, dispatch: () => null })

const reducer = (state, action) => {
    switch (action.type) {
        case 'CHANGE_LANGUAGE':
            return { ...state, language: action.payload }
        default:
            return state
    }
}

const LanguageProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState)
    return (
        <LanguageContext.Provider value={{ state, dispatch }}>
            {children}
        </LanguageContext.Provider>
    )
}

export { LanguageContext, LanguageProvider }
