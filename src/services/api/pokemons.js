import axios from 'axios';

// fetch all pokemons
export const fetchPokemons = async ({ limit, offset }) => {
    const { data } = await axios.get(`https://pokeapi.co/api/v2/pokemon/?limit=${limit}&offset=${offset}`)
    return data;
}

// fetch pokemon details
export const fetchPokemon = async ({ id }) => {
    const { data } = await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
    return data;
}

// fetch pokemon abilities
export const fetchAbilities = async ({ id }) => {
    const { data } = await axios.get(`https://pokeapi.co/api/v2/ability/${id}`)
    return data;
}

// fetch pokemon species
export const fetchSpecies = async ({ id }) => {
    const { data } = await axios.get(`https://pokeapi.co/api/v2/pokemon-species/${id}`)
    return data;
}

