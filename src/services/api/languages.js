import axios from 'axios';

// fetch languages
export const fetchLanguages = async () => {
    const { data } = await axios.get(`https://pokeapi.co/api/v2/language`)
    return data;
}
