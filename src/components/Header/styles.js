import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    height: 70px;
    align-items: center;
    justify-content: space-between;
    background-color: #f61b1c;
    padding: 0px 30px;
`;

export const Title = styled.p`
    color: #FFFFFF;
    font-size: 30px;
`;
