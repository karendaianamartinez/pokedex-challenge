import React, { useState } from 'react';

import { Container, Title } from './styles';

import { SelectLanguage } from '../SelectLanguage'

const Header = () => {
    return (
        <Container>
            <Title>POKEAPI</Title>
            <SelectLanguage name="Idiom" />
        </Container>
    );
}

export { Header }