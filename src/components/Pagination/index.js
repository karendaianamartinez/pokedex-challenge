import React from 'react';

import { Container, Button, Text } from './styles';

const Pagination = ({ totalDocuments, currentPage, pageCount, previousDisabled, handleNextPage, handlePreviousPage }) => {
    const totalPages = Math.ceil(totalDocuments / pageCount);

    return (
        <Container>
            <div>
                <Button onClick={handlePreviousPage} disabled={previousDisabled}>
                    Previous Page
                </Button>
                <Button onClick={handleNextPage}>
                    Next Page
                </Button>
            </div>

            <Text>{currentPage} / {totalPages}</Text>
        </Container>
    );
}

export { Pagination }