import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex: 1;
    flex-direction: row;
    align-items: center;
`;

export const Button = styled.button`
    cursor: pointer;
`;

export const Text = styled.p`
    margin-left: 10px;
    color: #FFFFFF;
`;
