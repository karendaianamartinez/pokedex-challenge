import React, { useState, useContext } from "react"
import { useQuery } from 'react-query';

import { LanguageContext } from '../../context'

import {
    Container,
    Checkbox,
    DropdownButton,
    Item,
    Label,
    Menu,
} from "./styles";


const SelectLanguage = () => {
    const [showMenu, setShowMenu] = useState(false);

    const { state: { language }, dispatch } = useContext(LanguageContext)

    const { isLoading, data: languages } = useQuery('languages');

    const showDropdownMenu = (e) => {
        e.preventDefault();
        setShowMenu(!showMenu);
    };

    const handleSetIdiom = (name) => {
        dispatch({ type: 'CHANGE_LANGUAGE', payload: name });
        setShowMenu(false);
    }

    return (
        <Container>
            <DropdownButton onClick={showDropdownMenu} disabled={isLoading}>{language}</DropdownButton>

            {showMenu && (
                <Menu>
                    {languages.results.map((language, i) => (
                        <Item key={i} onClick={() => handleSetIdiom(language.name)}>
                            <Label>{language.name}</Label>
                        </Item>
                    ))}
                </Menu>
            )}
        </Container>
    );
};

export { SelectLanguage }