import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const DropdownButton = styled.button`
    cursor: pointer;
    display: flex;
    justify-content: center;
    padding: 10px;
    border-radius: 50px;
    outline: none;
    font-family: GilroyMedium;
    font-size: large;
    align-items: center;
    color: ${props => (!props.disabled ? 'gray' : '#000000')};  height: 30px;
    width: 100px;
    border: 1px solid #b3b3b3;
    background: #FFFFFF;
    &:hover {
       background: gray;
       color: #FFFFFF
    };
`;

export const Menu = styled.div`
    display: flex;
    flex-direction: column;
    background: #FFFFFF;
    margin: 0;
    border-radius: 2px;
    max-height: 400px;
    position: absolute;
    top: 52px;
    width: 105px;
    z-index: 4;
    overflow: scroll;
    overflow-x: hidden;
    box-shadow: 0 2.8px 2.2px rgba(0, 0, 0, 0.034),
        0 6.7px 5.3px rgba(0, 0, 0, 0.048), 0 12.5px 10px rgba(0, 0, 0, 0.06),
        0 22.3px 17.9px rgba(0, 0, 0, 0.072), 0 41.8px 33.4px rgba(0, 0, 0, 0.086),
        0 100px 80px rgba(0, 0, 0, 0.12);
`;

export const Label = styled.p`
    text-align: left;
    color: #808080d9;
    font-size: 16px;
`;

export const Item = styled.div`
    display: flex;
    flex-direction: row;
    cursor: pointer;
    align-items: center;
    justify-content: center;
    padding: 2px;
    color: #585953;
    font-size: 14px;
    border-bottom: 0.5px #cccccc solid;
    font-family: GilroyMedium;
    font-weight: 500;
    height: 40px;
    border-radius: 2px;
    &:hover {
        background: rgb(237, 235, 210);
    }
`;
