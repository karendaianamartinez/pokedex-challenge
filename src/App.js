import React from 'react';
import { BrowserRouter } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";

import { LanguageProvider } from './context'
import { Routes } from './routes'

const queryClient = new QueryClient();


const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <LanguageProvider>
        <BrowserRouter >
          <Routes />
        </BrowserRouter>
      </LanguageProvider>
    </QueryClientProvider>

  );
}

export { App }