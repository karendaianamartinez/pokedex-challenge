import React from "react";
import {
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import { useQuery } from "react-query";

import { PokemonsView, PokemonDetails } from '../features'

import { Header } from '../components'

import { fetchLanguages } from '../services'

const routes = [
    {
        path: "/pokemons",
        component: PokemonsView
    },
    {
        path: "/pokemon-details/:id",
        component: PokemonDetails,
    }
];

const Routes = () => {
    const { isLoading } = useQuery('languages', fetchLanguages);

    if (isLoading) return null

    return (
        <>
            <Header />
            <Switch>
                {
                    routes.map((route, i) => (
                        <Route component={route.component} key={i} path={route.path} />
                    ))
                }

                <Route exact path="*">
                    <Redirect to="/pokemons" />
                </Route>
            </Switch>
        </>
    );
}

export { Routes }